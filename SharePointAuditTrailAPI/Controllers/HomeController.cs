﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.PlatformAbstractions;

namespace SharePointAuditTrailAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            // object with some dotnet runtime version information
            var runtimeFramework = PlatformServices.Default.Application.RuntimeFramework;
            return new string[] { runtimeFramework.ToString() };
        }
    }
}
